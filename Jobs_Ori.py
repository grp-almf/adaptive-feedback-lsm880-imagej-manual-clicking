from ij import IJ
from ij.gui import Roi
from ij.plugin import Duplicator
from ij.plugin.frame import RoiManager
from ij.process import ImageStatistics
from InteractiveFRAPDebug import StepMaker
import array
import random
import time
import math
from java.awt.event import MouseAdapter


def getMouseClickCoordinates(_img,mpar={}):

	imp = _img
	 
	class ML(MouseAdapter):
		mouseWasPressed = False;
		x = 0;
		y = 0;
 		
 		def mousePressed(self, event):
   			canvas = event.getSource()
   			#imp = canvas.getImage()
   			p = canvas.getCursorLoc()
   			self.mouseWasPressed = True
   			IJ.log("mouse was clicked at:"+str(p.x)+" "+str(p.y))
   			self.x = p.x
   			self.y = p.y
   			
		def getMouseStatus(self):
			return self.mouseWasPressed
			
   	IJ.setTool("point")
	listener = ML()
	imp.show()
	canvas = imp.getWindow().getCanvas()
	canvas.addMouseListener(listener)
	# wait for mouse click
	
	while(not listener.mouseWasPressed):
		time.sleep(0.2) # todo: how to make this better?
	
	#print listener.x,listener.y
	return listener.x,listener.y
	
   
@StepMaker(__name__=='__main__')#This line is needed in front of definition of each function, which is supposed to be tested with interactive dialog
def Smooth(_img,mpar={'width':1}):
	'''
	filters image with "Gaussian Blur" filter
	Keeps Original Image non-modified
	'''
	sm_im=Duplicator().run(_img)
	IJ.run(sm_im,"Gaussian Blur...", "sigma=%g" %(mpar['width']))#IJ.run is used as an example here. Can be any other command or list of commands
	#return smooth_im, #always return tuple, even it contains only one element
	return sm_im,

@StepMaker(__name__=='__main__')#This line is needed in front of definition of each function, which is supposed to be tested with interactive dialog
def FindBoundaries(_img,mpar={'smoothing_width':5,'threshold':20,'boundary_width':5}):
	'''
	Filters image to find cell boundaries
	Keeps Original Image non-modified
	'''
	cell_im=Duplicator().run(_img)
	# smooth
	IJ.run(cell_im,"Gaussian Blur...", "sigma=%g" %(mpar['smoothing_width']))
	# segment cells
	IJ.setThreshold(cell_im, mpar['threshold'], 255)
	Prefs.blackBackground = True
	IJ.run(cell_im, "Convert to Mask", "")
	IJ.run(cell_im, "Fill Holes", "")
	# erode 
	eroded_cell_im=Duplicator().run(cell_im)
	for i in range(1,mpar['boundary_width']):
	  IJ.run(eroded_cell_im, "Erode", "")
	boundary_im = ImageCalculator().run("Subtract create", cell_im, eroded_cell_im)

	return boundary_im,
	
@StepMaker(__name__=='__main__')
def LocalThresh(_img,mpar={'method':'Niblack','width':20,'thresh':5}):
	'''
	performs segmantation using "Auto Local threshold"
	Keeps an original Image non-modified
	mask image - binary image
	list of ROIs corresponds to the segmented objects 
	'''
	mask_im=Duplicator().run(_img)
	IJ.run(mask_im,"Auto Local Threshold",
		   "method=%s radius=%g parameter_1=%g parameter_2=0 white" %(mpar['method'],mpar['width'],mpar['thresh']))
	rMan=RoiManager.getInstance()
	rMan.runCommand("Reset")
	IJ.run(mask_im, "Analyze Particles...", "size=0-Infinity pixel circularity=0.00-1.00 show=Nothing exclude include add");
	
	mask_rois=rMan.getRoisAsArray()	
	return mask_im,mask_rois


@StepMaker(__name__=='__main__')
def FilterRois(_img,_rois,mpar={'eres_size_min':5,'eres_size_max':20,'eres_intens_min':120,'eres_intens_max':254,'eres_border_min':7}):   #'eres_dist_min':15,'eres_dist_max':200
	'''
	delete those rois which do not pass selection criteria from the initial array
	'''
	filt_rois=array.array(Roi)
	#	bool goodroi
	imw=_img.getWidth()
	imh=_img.getHeight()
	rMan=RoiManager.getInstance()
	rMan.runCommand("Reset")

	for r in _rois:
		_img.setRoi(r)
		goodroi=True
		rstat=_img.getStatistics(ImageStatistics.MIN_MAX|ImageStatistics.CENTER_OF_MASS|ImageStatistics.AREA)
		if ((rstat.area<mpar['eres_size_min']) or(rstat.area>mpar['eres_size_max'])):
			goodroi=False
		if ((rstat.max<mpar['eres_intens_min']) or(rstat.max>mpar['eres_intens_max'])):
			goodroi=False
		if ((rstat.xCenterOfMass<mpar['eres_border_min']) or (rstat.yCenterOfMass<mpar['eres_border_min']) or ((imw-rstat.xCenterOfMass)<mpar['eres_border_min']) or ((imh-rstat.yCenterOfMass)<mpar['eres_border_min'])):
			goodroi=False

			
		if (goodroi):	
			filt_rois.append(r)
			rMan.addRoi(r)
	return filt_rois,	


@StepMaker(__name__=='__main__')
def MostCentralRoi(_img,_rois,mpar={}):   #'eres_dist_min':15,'eres_dist_max':200
	'''
	return most central roi
	'''
	filt_rois=array.array(Roi)
	#	bool goodroi
	imw=_img.getWidth()
	imh=_img.getHeight()
	rMan=RoiManager.getInstance()
	rMan.runCommand("Reset")

	bestRoi = None
	minCenterDist = math.sqrt(pow(imw,2)+pow(imh,2))
	foundRoi = False
	
	for r in _rois:
		_img.setRoi(r)
		rstat=_img.getStatistics(ImageStatistics.MIN_MAX|ImageStatistics.CENTER_OF_MASS|ImageStatistics.AREA)
		centerDist = math.sqrt( pow(rstat.xCenterOfMass-imw/2,2) + pow(rstat.yCenterOfMass-imh/2,2 ) )
		if (centerDist < minCenterDist):
			minCenterDist = centerDist
			bestRoi = r
			foundRoi = True

	if foundRoi:
		print "minimal distance to center was",minCenterDist 	
		filt_rois.append(bestRoi)
		rMan.addRoi(bestRoi)
	else:	
		print "no roi found"
		
	return filt_rois,	


### WORKFLOWS

def Job_ClickOnSpot(img=None):
	if (img==None):
		IJ.log('No input image for the workflow')
	IJ.run(img, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel") #replace later with proper API function
	x,y = getMouseClickCoordinates(img)
	IJ.log("mouse was clicked at:"+str(x)+" "+str(y))
	return x,y


def Job_FindMostCentralSpot(img=None):
	if (img==None):
		IJ.log('No input image for the workflow')
	IJ.run(img, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel")#replace later with proper API function
	IJ.run("ROI Manager...", "")

		
	# smooth
	sm_im,=Smooth(_img=img)

	# find spots (tophat, difference of gaussians?!
	mask_im,mask_rois=LocalThresh(_img=sm_im, mpar={'method':'Niblack','width':20,'thresh':3})

	# filter spots according to size, distance and intensity
	#filt_rois,=FilterRois(_img=img, _rois=mask_rois, mpar={'eres_size_min':5,'eres_size_max':20,'eres_intens_min':60,'eres_intens_max':254,'eres_border_min':7})

	# filter spots according to their image center distance
	filt_rois,=MostCentralRoi(_img=img, _rois=mask_rois,mpar={})
	
	
	# return coordinates of the best roi
	if (filt_rois.__len__()==0):
		return None

	best_roi=random.choice(filt_rois)
	img.setRoi(best_roi)
	best_stat=img.getStatistics(ImageStatistics.CENTER_OF_MASS)
	return best_stat.xCenterOfMass,best_stat.yCenterOfMass

	

def Job_FindBoundarySpots(img=None):
	if (img==None):
		IJ.log('No input image for the workflow')
	IJ.run(img, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel") #replace later with proper API function
	IJ.run("ROI Manager...", "")

	
	# find nuclei
	# ....
		
	# find cell boundaries
	boundary_im,=FindBoundaries(_img=img)
	
	# find spots
	sm_im,=Smooth(_img=img)
	# tophat
	# ...
	# or difference of gaussian
	# ...
	mask_im,mask_rois=LocalThresh(_img=sm_im, mpar={'method':'Niblack','width':20,'thresh':3})
	
	# filter spots according to size, distance and intensity
	filt_rois,=FilterRois(_img=img, _rois=mask_rois, mpar={'eres_size_min':5,'eres_size_max':20,'eres_intens_min':60,'eres_intens_max':254,'eres_border_min':7})
	# filter spots at cell boundaries
	boundary_im.show()
	filt_boundary_rois,=FilterRois(_img=boundary_im, _rois=filt_rois, mpar={'eres_size_min':0,'eres_size_max':100000,'eres_intens_min':1,'eres_intens_max':255,'eres_border_min':0})
	
	if (filt_rois.__len__()==0):
		return None
	best_roi=random.choice(filt_rois)
	img.setRoi(best_roi)
	best_stat=img.getStatistics(ImageStatistics.CENTER_OF_MASS)
	return best_stat.xCenterOfMass,best_stat.yCenterOfMass
	

	
if __name__ == '__main__': #this will be implemented only if script is run directly, so we do not need additional boolean to know if script is run by macro
	print 'Let\'s test functions with the interface'
	imp = IJ.getImage()
	IJ.run(imp,'Select None','')
	#best_pos=BoundarySpotFinder(imp)
	#best_pos=Job_ClickOnSpot(imp)
	best_pos=Job_FindMostCentralSpot(imp)
	if(best_pos is not None):
		x,y=best_pos
		print "spot position at:",x,y
	else:
		print "no spot found."
			
		
