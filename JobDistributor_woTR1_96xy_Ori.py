'''
distributes image analysis jobs during automated FRAP experiments (Jython version)
assumes that job identifier in the image name is a unique label that determines what has to be done with an image
Authors: Christian Tischer, Aliaksandr Halavatyi
Last modified: 20.01.2014
'''

import os, sys
#sys.path.append('C:\\Users\\almf\\Desktop\\Fiji.app\\plugins\\AutoFRAPpack')

from os import path
from ij import IJ
from loci.plugins import BF
from loci.plugins.in import ImporterOptions
from loci.plugins.util import ROIHandler
from zeiss import ZeissKeys
zk=ZeissKeys()

#import modules performing image processing jobs
from Job1_FindFocus import MaxRefl
from Jobs_Ori import Job_ClickOnSpot
from Jobs_Ori import Job_FindMostCentralSpot



def ErrStop(msg):#!!!to implement
	'''
	!Stop script with an error
	
	'''
	IJ.log(msg)
	#sys.exit(msg)
	return

def submitCommandsToMicroscope(_codeM=None, _X=None, _Y=None, _Z=None, _roiT=None, _roiA=None, _roiX=None, _roiY=None, _errMsg=None):	
	#run("Read Write Windows Registry", "action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key=unit value=["+ unit +"] windows=REG_SZ")
	if(_X is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_xpos+' value=['+ str(_X) +'] windows=REG_SZ')
	if(_Y is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_ypos+' value=['+ str(_Y) +'] windows=REG_SZ')
	if(_Z is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_zpos+' value=['+ str(_Z) +'] windows=REG_SZ')
	if(_roiT is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_roitype+' value=['+ _roiT +'] windows=REG_SZ')
	if(_roiA is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_roiaim+' value=['+ _roiA +'] windows=REG_SZ')
	if(_roiX is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_roix+' value=['+ str(_roiX) +'] windows=REG_SZ')
	if(_roiY is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_roiy+' value=['+ str(_roiY) +'] windows=REG_SZ')
	if(_errMsg is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_errormsg+' value=['+ _errMsg +'] windows=REG_SZ')
	if(_codeM is not None): IJ.run('Read Write Windows Registry', 'action=write location=[HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro] key='+zk.subkey_codemic+' value=['+ _codeM +'] windows=REG_SZ')

def RunDistributor(fpth):
	IJ.log('RunDistributor') 
	img=BF.openImagePlus(fpth)[0]
	fnm=path.basename(fpth)
	# extract first plane
	#imp = Duplicator().run(bf, 1, 1, 1, 1, 1, 1);
	
	#now determine what is the image and what has to be done with it
	if (fnm.find('_AF_')>=0):		#what to do with the result of "Autofocus"
		IJ.log('This is AF image ****************************************************')
		IJ.log("Executing AF code...")
		zpos=MaxRefl(img)
		if(zpos is not None):
			submitCommandsToMicroscope(_codeM='focus', _X=19.5, _Y=0, _Z=zpos, _errMsg=None)		
		else:
			submitCommandsToMicroscope(_codeM='nothing', _errMsg='Autofocus not found')		
	elif (fnm.find('_AQ_')>=0): # AQUISITION
		IJ.log('This is AQ image ****************************************************')
		best_pos=Job_ClickOnSpot(img)
		if(best_pos is not None):
			x,y = best_pos
			IJ.log("FOUND BEST SPOT +++++++++++++++++++++++++++++++++++++++++++++")
			IJ.log(str(x))
			IJ.log(str(y))
			# launch zoomimaging
			center = (96-1)/2
			xroi=str(center)+', '+str(center+6) # centerX + radius
			yroi=str(center)+', '+str(center+6) # centerY + radius
			submitCommandsToMicroscope(_codeM='trigger1', _X=x, _Y=y, _Z=0, _errMsg=None, _roiT='circle', _roiA='bleach', _roiX=xroi, _roiY=yroi)		
		else:
			submitCommandsToMicroscope(_codeM='nothing', _errMsg='Suitable ERES is not found')		
	elif (fnm.find('_AL_')>=0): # ALTERNATIVE ACQUISITION
		IJ.log('This is AL image')
	elif (fnm.find('_TR1_')>=0): # TRIGGER2
		IJ.log('This is TR1 image ****************************************************')
		#best_pos=Job_FindMostCentralSpot(img)
	#close image at the end
	img.changes=False
	img.close() 	

if __name__ == '__main__':
	IJ.log('main')
	#newImagePath='D://Aliaksandr//Auto test 24012014//tst1_AF_W0001_P0001//tst1_AF_W0001_P0001_T0001.lsm'#uncomment this only for the test
	if ('newImagePath' not in dir()):
		ErrStop('Job Distributor: No file path found')
	fpth = newImagePath
	if (path.exists(fpth) and fpth.endswith('.lsm')):
		RunDistributor(fpth)