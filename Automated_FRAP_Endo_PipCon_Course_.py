#
#Macro to segment blobs and choose the nuclei that contains territories
#

from random import randint
import traceback
import re
import math
import sys
import warnings
from threading import Thread
from ij.plugin import RGBStackMerge
from ij import IJ
from ij import ImagePlus
from ij.plugin import ImageCalculator, ChannelSplitter
from ij.plugin import Duplicator
import segment_Particles
from ij.plugin import ZProjector, Slicer
from ij.plugin.frame import RoiManager
from ij.measure import Measurements
from ij.io import Opener
from ij.io import FileInfo
from ij.io import OpenDialog
import embl.almf.WindowsRegistry as WindowsRegistry
import automatedImgTools as autTool
#from embl.almf import WindowsRegistry #this does not work?? I can't access the methods
import sys
import os.path
import time
import copy
import random
from loci.plugins import BF
from ij.plugin import ZProjector

from ij.gui import ImageWindow
from javax.swing import (BoxLayout,  ImageIcon, JButton, JFrame, JPanel,
	JPasswordField, JLabel, JTextArea, JTextField, JScrollPane,
	SwingConstants, WindowConstants, SpringLayout, Box, JComboBox, JSeparator, SwingUtilities)
from java.awt import Component, GridLayout, BorderLayout, Dimension, Container, Color
from java.awt.event import ActionListener, WindowAdapter
from loci.plugins import BF
from Message import Message
from zeiss import ZeissKeys
import fileMonitoring
from fileMonitoring import DialogParameters
from org.apache.commons.io.monitor import FileAlterationListener, FileAlterationListenerAdaptor, FileAlterationMonitor, FileAlterationObserver
from java.awt.event import MouseAdapter
from threading import Thread
from java import lang

class ML(MouseAdapter):
	mouseWasPressed = False;
	x = 0;
	y = 0;
	def __init__(self, **kwargs):
		self.args = kwargs
	def mousePressed(self, event):
		canvas = event.getSource()
		#with middle mouse one can skip this
		if SwingUtilities.isMiddleMouseButton(event):
			executeTask_manualClick(None, **self.args)
			canvas.getImage().close()
			return
		p = canvas.getCursorLoc()
		self.mouseWasPressed = True
		IJ.log("mouse was clicked at:"+str(p.x)+" "+str(p.y))
		self.x = p.x
		self.y = p.y
		executeTask_manualClick([self.x, self.y], **self.args)
		canvas.getImage().close()
		
	def getMouseStatus(self):
		return self.mouseWasPressed

class locDialogParameters(DialogParameters, ActionListener):
	COMBO_FIELDS = dict(pip=[['Image'],['Trigger1','None', 'Default',  'Trigger2', 'Autofocus_AFS', 'Acquisition_AFS', 'Alt. acquisition_AFS',  'Trigger1_AFS', 'Trigger2_AFS']],
			task = [['Img. Task'],   ["1", "2", "3", "4", "5"]],
			command=[['Command'], ["nothing", "focus", "setFcsPos", "setFcsPos;focus", "trigger1", "trigger2"]], 
		  	
		  	channel=[['Main seg. Channel'],["1", "2", "3", "4"]],
		  	analysis = [["Analysis task"], [ "Spot detect","Cell detect", "Manual Click"]],
		  	seg_method=[['Seg. method'],  ["Default", "IsoData", "Intermodes", "Triangle", "Li", "MaxEntropy", "Otsu", "Shanbhag", "Mean", "Huang"]], 
		  	
		  	nrObj = [['CELL DETECT, nr of Cells'], [1]],
		  	background_rad = [["Background subtract rolling (px)"],[100]],    # Background subtract
		  	filter_med_rad=[['filer radius cells (px)'],[5]], 
		  	area_max=[['Exclude objects > (um2)'],[6000]], 
			area_min=[['Exclude objects < (um2)'], [20]], 
			
			minCirc = [["SPOT DETETCT, Min circularity"], [0.99]],
			minSpotSize = [["Min size (px)"], [20]],
			maxSpotSize = [["Max size (px)"], [60]],
			filter_gauss_rad = [["Filter radius (px)"], [3]], #gauss -radius
			frap_radius = [["Frap radius (px)"], [4]])
			
	COMBO_ORDER = ['pip', 'task', 'command', 'channel', 'analysis', 'seg_method', 'nrObj', 'background_rad', 'filter_med_rad', 'area_max', 'area_min', 
	 'minCirc', 'minSpotSize', 'maxSpotSize',   'filter_gauss_rad',  'frap_radius']
	COMBO_SEPARATOR = [2, 5, 10]
	JOBS_DICT = {'None':'', 'Default' : 'DE_', 'Trigger1': 'TR1_', 'Trigger2': 'TR2_', 'Autofocus_AFS':'AF_',
	'Acquisition_AFS':'AQ_', 'Alt. acquisition_AFS': 'AL_',  'Trigger1_AFS':'TR1_', 'Trigger2_AFS':'TR2_' }
	nrOfJobs = 2
	nrSeparators = len(COMBO_SEPARATOR)
	rois = [None]*nrOfJobs
	Segs = [None]*nrOfJobs
	Procs = [None]*nrOfJobs
	def __init__(self, macroname):
		super(locDialogParameters, self).__init__(macroname)
	
	class JCBoxEvent(ActionListener):
		def actionPerformed(self, event):
			cb = event.getSource()
			panel = cb.getParent()
			analyseTask = panel.getComponents()[4].getSelectedItem()
			if analyseTask  == "Cell detect": 
				for obj in panel.getComponents()[11:14]:
					obj.setEnabled(False)
			if analyseTask == "Spot detect": 
				for obj in panel.getComponents()[11:14]:
					obj.setEnabled(True)

	def actionPerformed(self, e):
		print e
	
	def runOnFile(self, afile, show = True, jobnr = None):
		afile = str(afile)
		if afile == '' or afile is None:
			od = OpenDialog("Choose image file", None)
			if od.getFileName() == None:
				return
			afile = os.path.join(od.getDirectory(), od.getFileName())
		if '.log' in afile:
			return
		
		try:
			zk = ZeissKeys()
			msg = Message()
			
			if self.jobs is None:
				IJ.showMessage("You need to first set the parameters")
			
			if all([job['pip']=='None' for job in self.jobs]):
				IJ.showMessage("Nothing to do! At least on job different than None") 
				return 0
			
			#create a copy otherwise self.jobs gets overwritten
			jobs = copy.deepcopy(self.jobs)
			random.seed()
			
			#create a roiManager in case one is missing and reset it
			roim = RoiManager.getInstance()
			if roim == None:
				roim = RoiManager()
			roim.runCommand("reset")
			
			for i, job in enumerate(self.jobs):
				jobl = job #not really necessary
				if jobl['pip'] == 'None':
					continue
				self.JOBS_DICT[jobl['pip']] + jobl['task'] + "_" 
				
				if jobnr is not None:
					if jobnr == i:
						#force opening
						imageDef = self.openFile(afile, self.getPrefix(self.JOBS_DICT[jobl['pip']], jobl['task']), 1)
					else: 
						continue
				else:
					imageDef = self.openFile(afile, self.getPrefix(self.JOBS_DICT[jobl['pip']], jobl['task']))
				
				jobl['channel'] = int(jobl['channel'])
				
				if imageDef is None:
					continue
				#clean up registry for errors
				IJ.log("Clean up errorMsg registry")
				IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_errormsg+" value=[""] windows=REG_SZ")
				
				[imageC, pixelSize, filepath, filename] = imageDef
				if jobl['channel'] > len(imageC):
					raise IOError('Expecting at least ' + str(jobl['channel']) + ' channels. Image has only ' + str(len(imageC)) + ' channel(s)')
				
				#switch color to Cyan for better visibility
				IJ.run(imageC[jobl['channel']-1], "Cyan", "")
				self.rois = [None]*self.nrOfJobs
				self.Segs = [None]*self.nrOfJobs
				self.Procs = [None]*self.nrOfJobs
				
				#transform area from um2 into pixelsize
				areas = [area/pixelSize/pixelSize  for area in [jobl['area_min'], jobl['area_max']]]
				
				#execute segmentation and task
				try:
					if jobl['analysis'] == "Cell detect":
						self.rois[i], self.Segs[i], self.Procs[i] = segmentChannel_cellDetect(imageC, **jobl)	
						particle = executeTask_cellDetect(self.Segs[i], self.rois[i], **jobl)
					if jobl['analysis'] == "Spot detect":
						self.rois[i], lt, self.Segs[i], self.Procs[i] = segmentChannel_spotDetect(imageC,  **jobl)
						particle = executeTask_spotDetect(self.Segs[i], self.rois[i], lt, **jobl)
						print "Spot detect is finished"
					if jobl['analysis'] == "Manual Click":
						listener = ML(**jobl)
						imp = imageC[jobl['channel']-1].duplicate()
						imp.show()

						canvas = imp.getWindow().getCanvas()
						canvas.addMouseListener(listener)
						IJ.setTool("point")
						continue
				except Exception, err:
					self.exitWithError(str(err))
					return
				
				if particle is None:
					self.Segs[i].show()
					self.Procs[i].show()
					continue
				
				if self.Procs[i].getNFrames()>1:
					self.Procs[i].setT(self.Procs[i].getNFrames())
				imgOut = autTool.createOutputImg(self.Procs[i], self.rois[i], particle)
				
				imgOut.show()

				imgOut2 = imgOut.duplicate()
				ip = imgOut2.getProcessor()
				imgOut2.setProcessor(ip)
				self.saveOutputImg(imgOut2, filepath, i+1)
				
				
			IJ.run("Collect Garbage", "")

		except BaseException, err:
			self.exitWithError(str(err))


def executeTask_cellDetect(image, rois,  **kwargs):
	'''execute task for microscope'''
	zk = ZeissKeys()
	msg = Message()
	if not rois:
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_codemic+" value=["+msg.CODE_NOTHING+"] windows=REG_SZ")
		return
	nrObj = kwargs['nrObj']*(kwargs['nrObj']<=len(rois))+len(rois)*(kwargs['nrObj']>len(rois))
	cent_x = []
	cent_y = []
	area = []
	for roi in rois:
		image.setRoi(roi)
		stats = image.getStatistics(Measurements.CENTROID + Measurements.AREA + Measurements.ELLIPSE)
		cent_x.append(round(stats.xCentroid))
		cent_y.append(round(stats.yCentroid))
		area.append(stats.area)
	
	particle = range(0,len(cent_x))
	random.shuffle(particle)
	particle = particle[0:int(nrObj)]

	msg.position = [[cent_x[i], cent_y[i]] for i in particle]
	if kwargs['task'] != 'nothing':
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roitype +" value=[] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roiaim+" value=[] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roix+" value=[] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roiy+" value=[] windows=REG_SZ")
		autTool.writePositionToRegistry(msg.position)
	IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_codemic+" value=["+ kwargs['command']+"] windows=REG_SZ")
	return particle

def executeTask_manualClick(point, **kwargs):
	zk = ZeissKeys()
	msg = Message()
	print "This point "
	print point
	if point is None:
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_codemic+" value=["+msg.CODE_NOTHING+"] windows=REG_SZ")
		return
	msg.position = [[point[0], point[1]]]
	if kwargs['command'] != 'nothing':
		autTool.writePositionToRegistry(None)
		roix = str(point[0]) + ", " +  str(point[0]+int(kwargs['frap_radius']))
		roiy = str(point[1]) + ", " +  str(point[1])
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roitype+" value=[circle] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roiaim+" value=[bleachAnalyse] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roix+" value=["+roix+"] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roiy+" value=["+roiy+"] windows=REG_SZ")
		if kwargs['command'] == 'focus':
			autTool.writePositionToRegistry(msg.position)
			
	IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_codemic+" value=["+kwargs['command']+"] windows=REG_SZ")


def executeTask_spotDetect(image, rois, lt, **kwargs):
	zk = ZeissKeys()
	msg = Message()
	if not rois:
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_codemic+" value=["+msg.CODE_NOTHING+"] windows=REG_SZ")
		return
	particle = randint(0, len(rois)-1)
	image.setRoi(rois[particle])
	stats = image.getStatistics(Measurements.CENTROID + Measurements.AREA + Measurements.ELLIPSE)
	cent_x = round(stats.xCentroid)
	cent_y = round(stats.yCentroid)
	msg.position = [[cent_x, cent_y]]

	if kwargs['command'] != 'nothing':
		autTool.writePositionToRegistry(None)
		roix = str(cent_x) + ", " +  str(cent_x+int(kwargs['frap_radius']))
		roiy = str(cent_y) + ", " +  str(cent_y)
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roitype+" value=[circle] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roiaim+" value=[bleachAnalyse] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roix+" value=["+roix+"] windows=REG_SZ")
		IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_roiy+" value=["+roiy+"] windows=REG_SZ")
		if kwargs['command'] == 'focus':
			autTool.writePositionToRegistry(msg.position)
		#except:

	
	IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\"+zk.regkey+"] key="+zk.subkey_codemic+" value=["+kwargs['command']+"] windows=REG_SZ")
	return [particle]

def segmentChannel_spotDetect(imageC,  **kwargs):
	'''
	Given a channel and parameters perform a segmentation
	imageC contains all channels of the image
	param: median_radius-gauss_radius, method, areamin(px), areamax(px), nr_watershed, exclude_boundary
	'''
	ch = kwargs['channel']
	IJ.run("Set Measurements...", " area centroid redirect=None decimal=2")
	if ch > len(imageC): 
		raise Exception('Expecting at least ' + str(ch) + ' channels. Image has only ' + str(len(imageC)) + ' channel(s)')
	try:
		IJ.run("Set Measurements...", " area centroid redirect=None decimal=2")
		IC = ImageCalculator()
		Sl = Slicer()
		Zp = ZProjector()
		Du  = Duplicator()
		imageProc = imageC[ch-1].duplicate()
		
		tp = imageProc.getNFrames()
		#filter and subtract background
		IJ.run(imageProc, "Median...", "radius="+str(kwargs['filter_med_rad']) + " stack")
		imageProc = IC.run("Subtract create stack", imageC[ch-1], imageProc);
		IJ.run(imageProc,"Enhance Contrast...", "saturated=0.4 normalize process_all");	
		IJ.run(imageProc, "Gaussian Blur...", "radius="+str(kwargs['filter_gauss_rad'])+ " stack")
		imageSeg = imageProc.duplicate()
		IJ.setAutoThreshold(imageSeg, kwargs['seg_method'] + " dark")
		IJ.run(imageSeg, "Convert to Mask", "background=Dark calculate black stack");
		IJ.run(imageSeg, "Options...", "iterations=1 count=7 black edm=Overwrite do=Erode stack")
		IJ.run(imageSeg, "Erode","stack")

		
		
		#this contains the life time of all spots
		spotLife =  imageSeg.duplicate()
		Zp.setImage(spotLife)
		Zp.setMethod(ZProjector.SUM_METHOD)
		Zp.doProjection()
		spotLife = Zp.getProjection()
		spotLife.setTitle("spotLifeTime")
		IJ.run(spotLife, "Divide...", "value=255 stack")

		#remove long lived spots (longer life than tp-2)
		spotLongLived = spotLife.duplicate()
		IJ.setThreshold(spotLongLived, tp-2,  tp)
		IJ.run(spotLongLived, "Convert to Mask", "")
		IJ.run(spotLongLived,"Options...", "iterations=3 count=1 black edm=Overwrite do=Dilate") 
		IJ.run(spotLongLived, "Invert", "")
		spotLongLived.setTitle("spotLongLived")
		goodSpots = IC.run("Multiply create 32-bit",spotLongLived, spotLife)
		
		#only the spots that also exist at the end are of interest
		imageSegLastFrame = Du.run(imageSeg, 1, 1, 1, 1, tp, tp)
		IJ.run(imageSegLastFrame, "Divide...", "value=255 stack");
		goodSpots = IC.run("Multiply create 32-bit",spotLife, imageSegLastFrame)
		#Keep only spots that have good circularity and size
		IJ.setThreshold(goodSpots, 1,tp)
		IJ.run(goodSpots, "Convert to Mask", "")
		IJ.run(goodSpots,"Analyze Particles...", "size="+str(kwargs['minSpotSize'])+"-"+str(kwargs['maxSpotSize']) +" circularity=" +  str(kwargs['minCirc']) +"-1.00 display exclude clear summarize add")
		roim = RoiManager.getInstance()
		imageSeg = spotLife
		imageProc.setTitle("Processed data")
		lt = []
		ltstd = []
		if roim == None:
			raise Exception('Fiji error segmentNuclei.py: no RoiManager!')
		if roim.getCount() > 0:
			rois = roim.getRoisAsArray()
			for i, roi in enumerate(rois):
				spotLife.setRoi(roi)
				stats = spotLife.getStatistics(Measurements.MIN_MAX + Measurements.STD_DEV)
				IJ.log("Found object " + str(i) + " with lifetime  " + str(stats.max) + " stdev " + str(stats.stdDev))
				lt.append(stats.max)
				ltstd.append(stats.stdDev)
			
			idx = ltstd.index(min(ltstd))
			
			return rois, lt, imageSeg, imageProc
		else:
			return None, None, imageSeg, imageProc
	except BaseException, err:
		traceback.print_exc()
		raise Exception('segmentChannel_spotDetect error' + str(err))


	
def segmentChannel_cellDetect(imageC, channel, **kwargs):
	'''
	Given a channel and parameters perform a segmentation
	imageC contains all channels of the image
	param: median_radius-gauss_radius, method, areamin(px), areamax(px), nr_watershed, exclude_boundary
	'''
	print channel
	IJ.run("Set Measurements...", " area centroid redirect=None decimal=2")
	if channel > len(imageC): 
		raise Exception('Expecting at least ' + str(channel) + ' channels. Image has only ' + str(len(imageC)) + ' channel(s)')
	try:		
		Zp = ZProjector()
		imageProc = imageC[channel-1].duplicate()
		#if it is a stack we do a sum projection
		try:
			Zp.setImage(imageProc)
			Zp.setMethod(ZProjector.SUM_METHOD)
			Zp.doProjection()
			imageProc = Zp.getProjection()
		except:
			IJ.log("this is not a stack")
		IJ.run(imageProc, "Subtract Background...", "rolling="+str(kwargs['background_rad']))
		IJ.run(imageProc, "Median...", "radius="+str(kwargs['filter_med_rad']))
		imageSeg = imageProc.duplicate()
		imageProc.setTitle("ProcessedCh" + str(channel))

		#IJ.run(imageSeg, "8-bit", "");
		IJ.setAutoThreshold(imageSeg, kwargs['seg_method']+" dark")
		IJ.run(imageSeg, "Convert to Mask", "");
		IJ.run(imageSeg, "Analyze Particles...", "size="+str(kwargs['area_min'])+"-"+str(kwargs['area_max'])+" circularity=0.0-1.00 show=Nothing display exclude clear summarize add");
		imageSeg.setTitle("SegmentedCh" + str(channel))
		
		roim = RoiManager.getInstance()
		if roim == None:
			raise Exception('Fiji error segmentNuclei.py: no RoiManager!')
		if roim.getCount() > 0:
			rois = roim.getRoisAsArray()
			return rois, imageSeg, imageProc
		else:
			return None, imageSeg, imageProc

	except BaseException, err:
		raise Exception('segmentChannel error' + str(err))

test =0



if test ==0:
	locDialogParameters('Automated_FRAP_Endo_PipCon')
	
if test ==1 :
	IJ.run("Close All", "");
	roim = RoiManager.getInstance()
	if roim == None:
		roim = RoiManager()
	
	roim.runCommand("reset");
	
	Du = Duplicator()
	#path1 = "D:\\Hetty\\150311\\LSM\\Test\\DE_W0001_P0001\\DE_2_W0001_P0001_T0001.lsm"
	path1 = r'D:\Hetty\150402_Sla1-mCherry_Abp1-GFP\LSM\DE_W0001_P0001\DE_2_W0001_P0001_T0001.lsm'
	
	#path1 = "D:\\Hetty\\150311\\TR1_1_2mCherry.czi"
	#path2 = "C:\\Users\\toni\\Science\\AndreaPiccoFrap\\141001\\Abp1_GFP\\abp1GFP_AQ_W0001_P0001\\abp1GFP_AQ_W0001_P0001_T0001\\abp1GFP_TR1_W0001_P0005\\abp1GFP_TR1_W0001_P0005_T0002\\abp1GFP_TR2_W0001_P0008\\abp1GFP_TR2_W0001_P0008_T0001.lsm"
	image = BF.openImagePlus(path1)[0]
	
	imageC = [Du.run(image, 1, 1, 1, 1, 1, image.getNFrames())];
	imageC[0].show()
	kwargs = {'nrObj': 1, 'area_max': 6000.0, 'minCirc': 0.99, 'command': 'trigger1', 'analysis': 'Cell detect', 'channel': 1, 'filter_med_rad': 5.0, 'area_min': 20.0, 'filter_gauss_rad': 3.0, 'frap_radius': 4.0, 'background_rad': 100.0, 'task': '1', 'area_ws': 1000.0, 'pip': 'Trigger1', 'seg_method': 'Default'}
	#kwargs = {'channel':"1", 'filter_med_rad': 5, 'seg_method': 'Yen', 'filter_gauss_rad': 3, 'minCirc':0.99, 'background_rad':100}
	kwargs['channel'] = int(kwargs['channel'])
	rois, imageSeg, imageProc = segmentChannel_cellDetect(imageC, **kwargs)
	particle = executeTask_cellDetect(imageSeg, rois,  **kwargs)
	#particle = executeTask_cellDetect(imageSeg, rois, lt, **kwargs)
	#if imageProc.getNFrames()>1:
	#	imageProc.setT(imageProc.getNFrames())
	#	imageProc = ImagePlus("outPutImg", imageProc.getProcessor())
	#imageProc.show()
	
	IJ.setForegroundColor(255,0,0)
	IJ.run(imageProc, "Enhance Contrast", "saturated=0")
	IJ.run(imageProc, "RGB Color", "")
	
	IJ.setForegroundColor(255,0,0)
	ip = imageProc.getProcessor()
	print particle
	ip.setColor(Color.red)
	for roi in rois:
		ip.draw(roi)
	ip.setColor(Color.green)
	for part in particle:
		ip.draw(rois[part])

	imageSeg.setProcessor(ip)
	imageSeg.show()


if test ==2 :
	IJ.run("Close All", "");
	roim = RoiManager.getInstance()
	if roim == None:
		roim = RoiManager()
	
	roim.runCommand("reset");
	
	Du = Duplicator()
	#path1 = "D:\\Hetty\\150311\\LSM\\Test\\DE_W0001_P0001\\DE_2_W0001_P0001_T0001.lsm"
	path1 = "D:\\Hetty\\150304\\10_mCherry.czi"
	path1 = r'D:\Hetty\150402_Sla1-mCherry_Abp1-GFP\LSM\DE_W0001_P0001\DE_2_W0001_P0001_T0001.lsm'
	
	#path1 = "D:\\Hetty\\150311\\TR1_1_2mCherry.czi"
	#path2 = "C:\\Users\\toni\\Science\\AndreaPiccoFrap\\141001\\Abp1_GFP\\abp1GFP_AQ_W0001_P0001\\abp1GFP_AQ_W0001_P0001_T0001\\abp1GFP_TR1_W0001_P0005\\abp1GFP_TR1_W0001_P0005_T0002\\abp1GFP_TR2_W0001_P0008\\abp1GFP_TR2_W0001_P0008_T0001.lsm"
	image = BF.openImagePlus(path1)[0]
	
	imageC = [Du.run(image, 1, 1, 1, 1, 1, image.getNFrames())];
	imageC[0].show()
	kwargs = {'area_max': 6000.0, 'minCirc': 0.99, 'command': 'trigger1', 'analysis': 'Cell detect', 'channel': 1, 'filter_med_rad': 5.0, 'area_min': 20.0, 'filter_gauss_rad': 3.0, 'frap_radius': 4.0, 'background_rad': 100.0, 'task': '1', 'area_ws': 1000.0, 'pip': 'Trigger1', 'seg_method': 'Default'}
	#kwargs = {'channel':"1", 'filter_med_rad': 5, 'seg_method': 'Yen', 'filter_gauss_rad': 3, 'minCirc':0.99, 'background_rad':100}
	kwargs['channel'] = int(kwargs['channel'])
	rois, lt, imageSeg, imageProc = segmentChannel_spotDetect(imageC, **kwargs)
	particle = executeTask_spotDetect(imageSeg, rois, lt, **kwargs)
	if imageProc.getNFrames()>1:
		imageProc.setT(imageProc.getNFrames())
		imageProc = ImagePlus("outPutImg", imageProc.getProcessor())
	#imageProc.show()
	
	IJ.setForegroundColor(255,0,0)
	IJ.run(imageProc, "Enhance Contrast", "saturated=0")
	IJ.run(imageProc, "RGB Color", "")
	
	IJ.setForegroundColor(255,0,0)
	ip = imageProc.getProcessor()

	ip.setColor(Color.red)
	for roi in rois:
		ip.draw(roi)
	ip.setColor(Color.green)
	if particle is not None:
		ip.draw(rois[particle])

	imageSeg.setProcessor(ip)
	imageSeg.show()
	#impOut = autTool.createOutputImg(imageProc, rois, 1)
	#impOut.show()


